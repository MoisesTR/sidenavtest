import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Menu} from '../models/Menu';
import * as data from '../assets/Menues.json';
import {MenuService} from './menu.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public menues: Menu[] = [];
  public menues2: Menu[] = [];

  constructor(private menuService: MenuService) {}

  ngOnInit(): void {
    this.getMenues();
  }

  getMenues() {
    this.menues = (<any>data).Menues;

    // Problem when data its coming from api
    // this.menuService.getMenuesByIdRol(1).subscribe(
    //       result => {
    //         this.menues = result.Menues;
    //       }
    //     );
  }

}
