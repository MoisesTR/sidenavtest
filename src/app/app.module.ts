import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MDBBootstrapModulesPro} from 'ng-uikit-pro-standard';
import {RouterModule, Routes} from '@angular/router';
import { TestingComponent } from './testing/testing.component';
import {MenuService} from './menu.service';

export const ROUTES: Routes = [
  {
    path: 'testing',
    component: TestingComponent,
    data: {
      titulo: 'Testing'
    }
  }
];

@NgModule({
  declarations: [
    AppComponent,
    TestingComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(ROUTES),
    MDBBootstrapModulesPro.forRoot()
  ],
  providers : [MenuService],
  bootstrap: [AppComponent]
})
export class AppModule { }
