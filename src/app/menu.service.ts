import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Global} from './global';
import {Observable} from 'rxjs/Observable';
import {Menu} from '../models/Menu';
import {map} from 'rxjs/operators';

@Injectable()
export class MenuService {

    public url: string;

    constructor(private _http: HttpClient) {
        this.url = Global.url;
    }

    getMenuesByIdRol(IdRol): Observable<any> {
        return this._http.get(this.url + 'menus/rol/' + IdRol);
    }
}
